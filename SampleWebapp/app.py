from flask import render_template , url_for, redirect, session
from flask import Flask, request
from pymongo import MongoClient
client = MongoClient('mongodb://mongo:27017/')

db = client.user_database
collection = db["user_collection"]




app = Flask("__name__")
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

@app.route('/')
def index():
    return "Hello rohit, This is sample application version:v1"

@app.route('/favorite')
def favorite():
    if session['Club'] == "RealMadrid":
        return render_template('Club1.html')
    elif session['Club'] == "ManchesterUnited":
        return render_template('Club2.html')
    elif session['Club'] == "ParisSaintGermain":
        return render_template('Club3.html')



@app.route('/select',methods=['GET', 'POST'])
def select():
    if request.method == 'POST':
        session['Club'] = request.form['Club']
        return redirect(url_for('favorite'))

    return render_template("select.html")


@app.route('/login', methods=['GET', 'POST'])
def login():
    user_data={}
    if request.method == 'POST':
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        user_data={
                 "username": session['username'],
                 "password": session['password'],
                 "Club": ""
                }

        if (session['username'] != "" and session['password'] != ""):
           collection.insert_one(user_data)

           return redirect(url_for('select'))
        else:
           return render_template("form.html")
    return render_template("form.html")


@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    session.pop('username', None)
    return redirect(url_for('index'))


if __name__ == "__main__":
    app.run(host='0.0.0.0')
