resource "azurerm_kubernetes_cluster" "main" {
  name                = var.aksname
  location            = var.location
  resource_group_name = var.rgname
  dns_prefix          = "${var.aksname}-dns"

  default_node_pool {
    name       = "agentpool"
    node_count = 1
    vm_size    = "Standard_D2_v2"
  }
 network_profile {
	  network_plugin = "azure"
	  dns_service_ip = "10.0.0.10"
	  docker_bridge_cidr = "172.17.0.1/16"
	  service_cidr = "10.0.0.0/16"

  }
  identity {
    type = "SystemAssigned"
  }

}

resource "null_resource" "main" {
provisioner "local-exec" {
  
  command = <<EOT
      az aks update -n ${var.aksname} -g ${var.rgname} --attach-acr ${var.acrname}
      
  EOT

  interpreter = ["/bin/bash", "-c"]
}
depends_on = [azurerm_container_registry.acr, azurerm_kubernetes_cluster.main]
}
