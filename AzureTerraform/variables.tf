variable "subid" {
	default = "SUB_ID"
}
variable "clientid" {
	default = "ClIENT_ID"
}
variable "tenantid" {
	default = "TENANT_ID"
}
variable "secret" {
	default = "SECRECT_KEY"
}
variable "accesskey" {
	default = "ACCESS_KEY"
}
variable "acrname" {
	default = "ACR_NAME"
}
variable "rgname" {
	default = "RG_NAME"
}
variable "location" {
	default = "LOCATION"
}
variable "aksname" {
	default = "AKS_NAME"
}
