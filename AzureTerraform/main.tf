terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.87.0"
    }
  }
}
terraform {
  backend "azurerm" {
    storage_account_name = "selfhostedsta"
    container_name       = "terraform"
    key                  = "terraform.tfstate"
    access_key = "ACCESS_KEY"
  }
}
provider "azurerm" {
  features {}

  subscription_id = var.subid
  client_id       = var.clientid
  client_secret   = var.secret
  tenant_id       = var.tenantid
}

